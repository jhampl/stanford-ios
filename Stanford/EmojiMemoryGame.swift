//
//  EmojiMemoryGame.swift
//  Stanford
//
//  Created by Josh Hampl on 8/12/21.
//

import Foundation

class EmojiMemoryGame {
    //Lots of syntactic cleanup 1:15 - 1:18 here
    private var model: MemoryGame<String> = EmojiMemoryGame.createMemoryGame()
    
    static func createMemoryGame() -> MemoryGame<String> {
        let emojis: Array<String> = ["😂","🎉","👀"]
        return MemoryGame<String>(pairsOfCards: emojis.count) { pairIndex in
            return emojis[pairIndex]
            
        }
    }
    
    // MARK: Access to the Model
    
    var cards: Array<MemoryGame<String>.Card> {
        return model.cards
    }
    // MARK: Intents
    
    func chooseCard(card: MemoryGame<String>.Card) { //Don't forget we have to fully qualify the Card
        model.choose(card: card) //This could be SQL
    }
}
