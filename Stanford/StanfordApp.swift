//
//  StanfordApp.swift
//  Stanford
//
//  Created by Josh Hampl on 7/22/21.
//

import SwiftUI

@main
struct StanfordApp: App {
    let game = EmojiMemoryGame()
    var body: some Scene {
        WindowGroup {
            ContentView(viewModel: game)
        }
    }
}
