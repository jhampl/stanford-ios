#  CS193p iPhone Application Development Spring 2020 - Stanford

[This is a link to the video in case you forget](https://www.youtube.com/watch?v=yOhyOpXvaec&list=PLpGHT1n4-mAtTj9oywMWoBx0dCGd51_yG)

## Lecture 1: Introduction
- It's the norm to label function arguments. Eg. `return RoundedRectange(cornerRadius: 10.0)`
- Basically everything is a view, and everything returns a view
- `ZStack()` organizes things on the z-axis. ZStack is also `some view`  Don't need the return statement inside ZStack
- Everything in the `ZStack` must be a view. So `RoundedRectangle` is a view; it also behaves like a shape, which is why it can be `stroke()`'d
- If you apply a foregroundColor to the ZStack parent view, it will apply it to every view underneath (is this a global thing?? Seems like it). Can be overridden by applying the same property to a view below it. Basically view inheritance
- I guess you can't stroke and fill at the same time?
- `ForEach` is not a layout function, it is purely an iterator
- `HStack()` arranges things horizontally
- `ZStack()` `HStack` and `ForEach` are known as *View Combiners*
- `HStack` has an implied spacing, usually this is left as-is to use standard padding and spacing across iOS
- Swift has a really weird syntax where, if the last argument is a curly brace function, you can close the 1st argument and then float the curly brace. Really does reduce the amount of parenthesis though.
```
ForEach(0..<4, content: {
    ZStack(content: {
        RoundedRectangle()
    })
})

// can become

ForEach(0..<4) {
    ZStack {
        RoundedRectangle()
    }
}
```
- If *no* arguments, then can remove parends. Seems like this is just syntactic sugar
- Swift is strongly typed and all variables require a default initialization value

## Lecture 2: MVVM and Type System

### MVVM
- Model is UI independent. Used for data & logic. Model always represents true state of data. Could be db
- View always reflects the model. Can always ask the view what it is and it returns exactly what is in the model. It's stateless
- Data always flows from model to view. view is read only. In facts structs, of which a view is one, is read-only by default
- view is declarative, not imperative
- at any time you can say "make it look like the model" - reactive programming
- the viewmodel binds the view to the model (this is the -VM in MVVM)
- simpler data structures than the model. model might be a complex db. sounds like the VM might be simpler representative structs that make it easier for the view to do its thing
- VM makes the updating happen: waits to notice changes in the model, then publishes "something has changed" 
- The view model DOES NOT TALK to any views - it only says something has changed
- View responds to "something has changed" and pulls data and rebuilds. View subscribes to publication (think RxJs)
- VM also "processes intent" - what the user wants to do
- Exmaple intent: "i want to flip a card"
- VM makes functions available to the view which correspond to uesr intents on screen. e.g `flipCard()`
- Intents aren't super formalized. You'd just put them maybe at the bottom of your file with a comment block above saying "these are the intents"
- VM modifies the Model to express the user's intent
- **When I make this app, I need to have this slide with 3 boxes timestamp 14:40, and then for every screen I need to plan everything and trace it through these boxes. Makes so much sense when this guy explains it but DEFINITELY something I need to think about and get used to. **

### Structs
- both structs and class have pretty much same syntax (both contain vars)
- `let` is a constant
- each function argument can have two labels, eg

```swift
func multiply(_ operand: Int, by otherOperand: Int) -> Int {
    return operant * otherOperand
}
```
- _ means no label is required by the caller! "Unused"
- The difference between the 2 labels is that the first one is used by function callers and the second one is used internally
- Put together, this means a call can do `multiply(5, by: 6)` (note the no label)
- structs and classes have initializers to construct
- structs are value typed and classes are reference types (**see slide for a helpful table**)
- Should think "should I use a struct or class for this"?
- structs support functional programming
- Structs have an implicit init that can be used many times whereas classes you almost always have to write your own, becuase classes don't initialize any vars
- structs are the go-to data structure
- the viewModel is always a class
- views are protocols, covered next week
- arrays are generics, and are **type agnostic**

example array declaration - type agnostic
```swift
struct Array<Element> {
    fun append(_ element: Element) { ... }
}
```
- type `Element` is the generic, don't care type
- can also do `Array<Int>`
- functions have types too (**see slide**)
- can declare a variable of type function `var foo: (Double) -> Void` 
- when you pass something through a function type it doesn't need labels (am I sure?)
- 
### Closures
- inlined passing of function as parameters to other functions
- does some nice things to handle local variables, etc (more than just inlining)
- will be covered more next week

Demo time!

### Demo notes
- print("Card picked is: \(card)")
- `\(card)` is swift string interpolation. It's very good at printing things, will print all vars in a struct
- ViewModel knows what's going to be drawn on screen
- You might use a struct to wrap a SQL db
- Biggest advantage of a class, and why the viewmodel is a class, is because it's shareable -- it lives on the heap and there are pointers to it; as opposed to structs which getting passed around. Many views are going to want to look through the "portal" of the viewmodel into the model
- But this is also a weakness
- Marking a class var as private `private var` means it's only accessible to the owning class
- `private(set) var` is like a glass door - only the class can modify the model, but everyone else can see the model. **preferred approach** it sounds like. But still nothing can interact with the model, which is where intent comes in. Add class functions to work as intents. Basically setters and getters
- It's the viewmodel's job to present the model to the view in a way that's easily consumable
- It's up to the instantiator to initalize vars if they're left undeclared 
- `Init()` is an initialize function. Don't need to say `func`. Can be overloaded
- He went over some complex function passing that I need to rewatch or something 1 hour to 1:15
- Inlining a function is called a closure, captures surrounding information too
- Lots of syntactic cleanup 1:15 - 1:18. Don't understand all of the steps
- Very functional programming related. Will be passing many functions as agruments to other function
- `Static func` makes a function part of the type rather than part of the instance. Good way to solve initialization issues before self is available
- The iterator in a `forEach` must be `identifiable`. Can't do `forEach(viewModel.cards)` (an array). 
- Can make something identifiable with "constrains and gains" - if you constrain a struct in a certain way, you gain additional behavior (next lecture)
- 1:34 `struct Card: Identifiable { .... }` - Identifiable is a `protocol`
- When identifiable, must have a var called `id` of any type
- It's handy to set default values when intializing struct vars, so it cleans up future initializations
- Oh I get it now - `index in` iterates a `range`
- When using an identifiable struct, iterate with `struct in` . e.g `card in { ... }` - but does this mean anything? Or is he just using "card" as the iterator name?

